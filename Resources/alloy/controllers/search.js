function Controller() {
    function buttonClick(e) {
        $.search.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    var $ = this, exports = {};
    $.__views.search = A$(Ti.UI.createWindow({
        backgroundColor: "white",
        navBarHidden: !1,
        title: "Segunda Tela",
        id: "search"
    }), "Window", null);
    $.addTopLevelView($.__views.search);
    $.__views.labelTeste = A$(Ti.UI.createLabel({
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        top: 0,
        text: "Testando 1 2 3",
        id: "labelTeste"
    }), "Label", $.__views.search);
    $.__views.search.add($.__views.labelTeste);
    var __alloyId1 = [];
    $.__views.searchBar = A$(Ti.UI.createSearchBar({
        barColor: "#000",
        showCancel: "true",
        id: "searchBar"
    }), "SearchBar", null);
    $.__views.tableSearch = A$(Ti.UI.createTableView({
        width: Ti.UI.FILL,
        height: Ti.UI.FIT,
        top: 40,
        search: $.__views.searchBar,
        id: "tableSearch"
    }), "TableView", $.__views.search);
    $.__views.search.add($.__views.tableSearch);
    $.__views.btnBack = A$(Ti.UI.createButton({
        left: 10,
        right: 10,
        bottom: 0,
        title: "Voltar",
        id: "btnBack"
    }), "Button", $.__views.search);
    $.__views.search.add($.__views.btnBack);
    $.__views.btnBack.on("click", buttonClick);
    _.extend($, $.__views);
    $.tableSearch.setData([ {
        title: "teste 1"
    }, {
        title: "teste 2"
    }, {
        title: "teste 3"
    } ]);
    $.search.open({
        modal: !0
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A;

module.exports = Controller;