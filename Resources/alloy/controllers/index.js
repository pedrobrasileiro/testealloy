function Controller() {
    function buttonClick(e) {
        if ($.tfUser.value != "") if ($.tfPassword.value != "") Alloy.createController("search"); else {
            alert("Password mandatory!");
            $.tfPassword.focus();
        } else {
            alert("User mandatory!");
            $.tfUser.focus();
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    var $ = this, exports = {};
    $.__views.index = A$(Ti.UI.createWindow({
        backgroundColor: "white",
        layout: "vertical",
        id: "index"
    }), "Window", null);
    $.addTopLevelView($.__views.index);
    $.__views.tfUser = A$(Ti.UI.createTextField({
        top: 10,
        left: 10,
        right: 10,
        height: Ti.UI.SIZE,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        id: "tfUser",
        hintText: "User"
    }), "TextField", $.__views.index);
    $.__views.index.add($.__views.tfUser);
    $.__views.tfPassword = A$(Ti.UI.createTextField({
        top: 10,
        left: 10,
        right: 10,
        height: Ti.UI.SIZE,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        id: "tfPassword",
        hintText: "Password",
        passwordMask: "true"
    }), "TextField", $.__views.index);
    $.__views.index.add($.__views.tfPassword);
    $.__views.btnLogin = A$(Ti.UI.createButton({
        color: "#000",
        top: 100,
        left: 10,
        right: 10,
        height: Ti.UI.SIZE,
        title: "Login",
        id: "btnLogin"
    }), "Button", $.__views.index);
    $.__views.index.add($.__views.btnLogin);
    $.__views.btnLogin.on("click", buttonClick);
    _.extend($, $.__views);
    $.index.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A;

module.exports = Controller;